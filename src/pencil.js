export default class Pencil {
  constructor(pointDurability = null, length, eraserDurability = null) {
    // FIX: validate inputs
    this._paper = null;
    this._maxDurability = pointDurability;
    this._pointDurability = this._maxDurability;
    this._length = length;
    this._eraserDurability = eraserDurability;
  }

  // calculate amount of point durability needed for given character
  static charPointConsumption(char) {
    // FIX: validate char length

    // ignore whitespace
    if (char.trim() === '') return 0;

    // If character doesn't have case, count it as 1
    if (char.toLowerCase() === char.toUpperCase()) return 1;

    // if it's uppercase, consume 2 durability
    if (char === char.toUpperCase()) return 2;

    // if it's lowercase, consume 1 durability
    if (char === char.toLowerCase()) return 1;

    // if something has gone wrong, it's still a character so consume 1
    return 1;
  }

  usePaper(paper) {
    // FIX: this should validate paper object and throw error if invalid
    this._paper = paper;
  }

  write(text, startPosition = null) {
    // can only start writing in previously erased positions
    if (startPosition && !this._paper.isErased(startPosition)) return;

    [...text].forEach((char, index) => {
      // if given a starting position, calculate this character's position
      const charPosition = startPosition === null ? startPosition : startPosition + index;
      if (this.pointDurability === null) {
        return this._paper.writeChar(char, charPosition);
      }

      // consume the graphite to attempt to write this character
      if (this.consumePoint(char)) {
        return this._paper.writeChar(char, charPosition);
      }

      return this._paper.writeChar(' ', charPosition);
    });
  }

  sharpen() {
    if (this.length <= 0) return;
    this._length -= 1;
    this._pointDurability = this._maxDurability;
  }

  erase(text) {
    // if text is found in paper
    const pos = this._paper.contents.lastIndexOf(text);
    if (pos >= 0) {
      // loop through backwards and attempt to erase each character
      [...text].reverse().forEach((char, index) => {
        // begin at starting position of searched text plus word length minus characters erased
        const charPos = pos + (text.length - 1 - index);
        if (this.consumeEraser(this._paper.contents[charPos])) {
          this._paper.erase(charPos);
        }
        return charPos;
      });
    }
    return pos;
  }

  /**
  * Consumes eraser durability for a given character and returns if amount enough for request
  * @param    {String} char     single character that is being written
  * @return   {Boolean}         was there enough eraser to fill request
  */
  consumeEraser(char) {
    // don't consume if pencil doesn't use eraser durability
    if (this._eraserDurability === null) return true;

    // don't consume for whitespace
    if (char.trim().length === 0) return true;

    // FIX: validate char length
    if (this._eraserDurability > 0) {
      this._eraserDurability -= 1;
      return true;
    }

    return false;
  }

  /**
  * Consumes point durability for a given character and returns if amount enough for request
  * @param    {String} char     single character that is being written
  * @return   {Boolean}         was there enough graphite to fill request
  */
  consumePoint(char) {
    // FIX: validate char length
    const cost = Pencil.charPointConsumption(char);
    this._pointDurability -= cost;
    if (this._pointDurability < 0) {
      this._pointDurability = 0;
      return false;
    }

    return true;
  }

  get eraserDurability() {
    return this._eraserDurability;
  }

  get pointDurability() {
    return this._pointDurability;
  }

  get length() {
    return this._length;
  }
}
