export default class Paper {
  // used to denote an erased position
  static get ERASED() { return '{e}'; }

  static get ERASED_CHAR() { return ' '; }

  // used to denote a character collision
  static get COLLISION() { return '{@}'; }

  static get COLLISION_CHAR() { return '@'; }

  constructor() {
    // contents stored internally as a char array to be encoding independant when accessing by index
    this._contents = [];
  }

  isErased(pos) {
    return this._contents[pos] === Paper.ERASED;
  }

  // write text to the paper
  writeChar(char, pos = null) {
    // only allow writing one character at a time
    if (char.length !== 1) return; // FIX: should throw an error

    // if a position wasn't given, character should be appended
    if (pos === null) {
      this._contents.push(char);
      return;
    }

    // FIX: validate pos exists in array
    const currentChar = this._contents[pos];

    // allow overwriting whitespace
    if (currentChar.trim() === '' || currentChar === Paper.ERASED) {
      this._contents[pos] = char;
      return;
    }

    // printable character already exists in this position, use the collision character
    this._contents[pos] = Paper.COLLISION;
  }

  erase(position) {
    // FIX: validate position exists

    // mark this position as erased
    this._contents[position] = Paper.ERASED;
  }

  // return the text written to the paper
  get contents() {
    // put the array back together and replace erased char with normal space
    // eslint-disable-next-line no-useless-escape
    return this._contents.map((char) => {
      if (char === Paper.ERASED) return Paper.ERASED_CHAR;
      if (char === Paper.COLLISION) return Paper.COLLISION_CHAR;
      return char;
    }).join('');
  }
}
