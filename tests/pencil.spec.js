import { expect } from 'chai';
import Pencil from '../src/pencil';
import Paper from '../src/paper';

describe('Pencil', () => {
  it('writes text to paper', () => {
    const message = "Ce n'est pas du papier";
    const pencil = new Pencil();
    const paper = new Paper();
    pencil.usePaper(paper);
    pencil.write(message);
    expect(paper.contents).to.equal(message);
  });

  it('appends subsequent text written', () => {
    const message1 = "Ce n'est pas";
    const message2 = ' du papier';
    const pencil = new Pencil();
    const paper = new Paper();
    pencil.usePaper(paper);
    pencil.write(message1);
    pencil.write(message2);
    expect(paper.contents).to.equal(message1 + message2);
  });

  describe('point', () => {
    it('can have a given a durability', () => {
      const pointValue = 10;
      const pencil = new Pencil(pointValue);
      expect(pencil.pointDurability).to.equal(pointValue);
    });

    describe('durability', () => {
      it('decreases two points when writing an uppercase character', () => {
        const pointValue = 5;
        const message = 'AZ';
        const pencil = new Pencil(pointValue);
        const paper = new Paper();
        pencil.usePaper(paper);
        pencil.write(message);
        expect(paper.contents).to.equal(message);
        expect(pencil.pointDurability).to.equal(pointValue - (message.length * 2));
      });

      it('decreases one point for each lowercase character', () => {
        const pointValue = 5;
        const message = 'az';
        const pencil = new Pencil(pointValue);
        const paper = new Paper();
        pencil.usePaper(paper);
        pencil.write(message);
        expect(paper.contents).to.equal(message);
        expect(pencil.pointDurability).to.equal(pointValue - message.length);
      });

      it('decreases one point for each character without a case', () => {
        const pointValue = 5;
        const message = '2!,';
        const pencil = new Pencil(pointValue);
        const paper = new Paper();
        pencil.usePaper(paper);
        pencil.write(message);
        expect(paper.contents).to.equal(message);
        expect(pencil.pointDurability).to.equal(pointValue - message.length);
      });

      it('does not decrease with whitespace', () => {
        const pointValue = 5;
        const whitespace = ' \f\n\r\t\v\u00a0\u1680\u2000\u200a\u2028\u2029\u202f\u205f\u3000\ufeff';
        const message = `a${whitespace}z`;
        const expectedCost = 2;
        const pencil = new Pencil(pointValue);
        const paper = new Paper();
        pencil.usePaper(paper);
        pencil.write(message);
        expect(paper.contents).to.equal(message);
        expect(pencil.pointDurability).to.equal(pointValue - expectedCost);
      });

      it('reaches zero then writes whitespace', () => {
        const pointValue = 5;
        const message = 'ABCtest';
        const expectedMessage = 'AB     ';
        const pencil = new Pencil(pointValue);
        const paper = new Paper();
        pencil.usePaper(paper);
        pencil.write(message);
        expect(paper.contents).to.equal(expectedMessage);
        expect(pencil.pointDurability).to.equal(0);
      });
    });

    describe('sharpen', () => {
      it('resets the durability', () => {
        const pointValue = 6;
        const message = 'aaaaa';
        const pencil = new Pencil(pointValue);
        const paper = new Paper();
        pencil.usePaper(paper);
        pencil.write(message);
        expect(pencil.pointDurability).to.equal(1);
        pencil.sharpen();
        expect(pencil.pointDurability).to.equal(6);
      });

      it('decreases the length', () => {
        const length = 5;
        const pencil = new Pencil(null, length);
        expect(pencil.length).to.equal(5);
        pencil.sharpen();
        expect(pencil.length).to.equal(4);
        pencil.sharpen();
        expect(pencil.length).to.equal(3);
      });

      it("can't occur once length is zero", () => {
        const length = 1;
        const message = 'Test';
        const pointValue = 5;
        const pencil = new Pencil(pointValue, length);
        pencil.usePaper(new Paper());
        expect(pencil.length).to.equal(1);
        pencil.write(message);
        expect(pencil.pointDurability).to.equal(0);
        pencil.sharpen();
        expect(pencil.length).to.equal(0);
        expect(pencil.pointDurability).to.equal(pointValue);
        pencil.write(message);
        pencil.sharpen();
        expect(pencil.length).to.equal(0);
        expect(pencil.pointDurability).to.equal(0);
      });
    });
  });

  describe('eraser', () => {
    it('always replaces the last occurence of given text with whitespace', () => {
      const pencil = new Pencil(null, 10);
      const paper = new Paper();
      const message = 'How much wood would a woodchuck chuck if a woodchuck could chuck wood?';
      const erase = 'chuck';
      const expectedMessage1 = 'How much wood would a woodchuck chuck if a woodchuck could       wood?';
      const expectedMessage2 = 'How much wood would a woodchuck chuck if a wood      could       wood?';
      pencil.usePaper(paper);
      pencil.write(message);
      pencil.erase(erase);
      expect(paper.contents).to.equal(expectedMessage1);
      pencil.erase(erase);
      expect(paper.contents).to.equal(expectedMessage2);
    });

    it('decreases in durability by 1 for each character erased', () => {
      const eraserValue = 20;
      const pencil = new Pencil(null, 10, eraserValue);
      const paper = new Paper();
      const message = 'How much wood would a woodchuck chuck if a woodchuck could chuck wood?';
      const erase = 'chuck';
      pencil.usePaper(paper);
      pencil.write(message);
      pencil.erase(erase);
      expect(pencil.eraserDurability).to.equal(eraserValue - erase.length);
    });

    it('erases backwards one character at a time', () => {
      const eraserValue = 4;
      const pencil = new Pencil(null, 10, eraserValue);
      const paper = new Paper();
      const message = 'How much wood would a woodchuck chuck if a woodchuck could chuck wood?';
      const expectedMessage = 'How much wood would a woodchuck chuck if a woodchuck could c     wood?';
      const erase = 'chuck';
      pencil.usePaper(paper);
      pencil.write(message);
      pencil.erase(erase);
      expect(pencil.eraserDurability).to.equal(0);
      expect(paper.contents).to.equal(expectedMessage);
    });

    it('only decreases in durability for visible characters', () => {
      const eraserValue = 7;
      const pencil = new Pencil(null, 10, eraserValue);
      const paper = new Paper();
      const message = 'How much wood would a woodchuck chuck if a woodchuck could chuck wood?';
      const expectedMessage = 'How much wood would a woodchuck chuck if a woodchuck cou         wood?';
      const erase = 'could chuck';
      pencil.usePaper(paper);
      pencil.write(message);
      pencil.erase(erase);
      expect(pencil.eraserDurability).to.equal(0);
      expect(paper.contents).to.equal(expectedMessage);
    });
  });

  describe('edits', () => {
    it('can start writing text in previously erased areas', () => {
      const message = 'This is an apple.';
      const expectedMessage = 'This is an orange';
      const pencil = new Pencil();
      const paper = new Paper();
      pencil.usePaper(paper);
      pencil.write(message);
      const pos = pencil.erase('apple.');
      pencil.write('orange', pos);
      expect(paper.contents).to.equal(expectedMessage);
    });

    it('cannot write text in area that has not been erased', () => {
      const message = 'This is an apple.';
      const pencil = new Pencil();
      const paper = new Paper();
      pencil.usePaper(paper);
      pencil.write(message);
      pencil.write('orange', message.indexOf('apple.'));
      expect(paper.contents).to.equal(message);
    });

    it('display an @ for overwritten non-whitespace characters', () => {
      const message = 'An apple a day keeps the doctor away';
      const expectedMessage = 'An artich@k@ay keeps the doctor away';
      const pencil = new Pencil();
      const paper = new Paper();
      pencil.usePaper(paper);
      pencil.write(message);
      const pos = pencil.erase('apple');
      pencil.write('artichoke', pos);
      expect(paper.contents).to.equal(expectedMessage);
    });
  });
});
