# Accenture Code Kata - Pencil Durability

## Running tests

If the system has NodeJS v14 or v15 and Yarn installed, the tests can be run with the following command:

```sh
yarn # install modules
yarn run test # run tests
```

If the system has Docker and Docker Compose installed, the tests can still be run (though much slower) with the following command:

```sh
docker-compose up # build container and run tests in it
```

## Assumptions

Certain areas of the instructions were not entirely clear. Normally these would be determined ahead of time before beginning coding, but for simplicity the following assumptions were made about the kata:

- Characters that do not have uppercase or lowercase, such as punctuation and numbers, have been decided to reduce the pencil durability by 1 point.
- Since requirements state that pencil "can" be given a point/eraser durability, when either is not given it will always work regardless of amount written/erased.
- if a pencil attempts to write an uppercase character while having a durability of 1, the durability is reduced to 0 but the character is not written (rather than save the durability for the next lowercase letter)
- Following the requirements precisely, a pencil can only begin writing in a space that has previously been erased and will fail to write if started in a position that was not previously erased

## Notes

The code was written taking only one requested behavior into account at a time in the order they appear in the kata without looking forward at later requirements to give a better idea of how I would develop something with requirements being added over time.

For simplicity, attempts to violate the usage in the requirements fail silently rather than throwing an error.
